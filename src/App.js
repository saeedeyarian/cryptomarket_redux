import { Routes,Route } from "react-router-dom";
//redux
import  { Provider }  from "react-redux";
import store from "./components/redux/store";

//css
import './App.css';

//components
import CryptoMarket from "./components/CryptoMarket";
import Navbar from "./components/Navbar";
import EuroCryptoMarket from "./components/EuroCryptoMarket"
import Details from "./components/CryptoDetails";

function App() {
  return (
    <Provider store={store}>
      <Navbar/>
      <Routes>
        <Route path="/" element={<CryptoMarket/>}/>
        <Route path="/Euro" element={<EuroCryptoMarket/>}/>
        <Route path="/:id" element={<Details/>}/>
      </Routes>
    </Provider>
  );
}

export default App;
