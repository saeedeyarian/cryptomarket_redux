import React, { useState } from 'react';
import { Link } from 'react-router-dom';

//css
import styles from "./scss/Navbar.module.css";

const Navbar = () => {

    const [options, setOptions] = useState(true)
    const showOptions = () => {
        setOptions(!options)
    }

    return (
        <div className={styles.container}>
            <div className={styles.textContainer}>
                <Link to="/">Crypto Hunter</Link>
            </div>
            <div className={styles.selectContainer}>
                <div className={styles.selection}>
                    {
                        options ?
                        <p>USD</p> :
                        <p>EUR</p>
                    }
                </div>
                <div className={styles.options}>
                    <Link to="/"><p onClick={showOptions}>USD</p></Link>
                    <Link to="/EURO"><p onClick={showOptions}>EUR</p></Link>
                </div>
            </div>
        </div>
    );
};

export default Navbar;