import React,{ useEffect } from 'react';

//redux
import { useSelector,useDispatch } from "react-redux"

//img
import spinner from "../assets/Spinner.svg";

//components
import fetchEuroCrypto from './redux/euro/euroCryptoAction';
import EuroCrypto from "./shared/EuroCrypto"

//css
import styles from "./scss/CryptoMarket.module.css";

const CryptoMarket = () => {

    const dispatch = useDispatch();
    const euroCryptoState = useSelector(state => state.euroCryptoState);

    useEffect( () => {
        if (!euroCryptoState.cryptos.length) {
            dispatch(fetchEuroCrypto())
        }
    },[])

    return (
        <div className={styles.container}>
            <div className={styles.title}>
                <p style={{width:"25%"}}>name</p>
                <p>symbol</p>
                <p>current price</p>
                <p>price change</p>
                <p>market cap</p>
            </div>
            <div classsName={styles.cryptoContainer}>
                {euroCryptoState.loading ?
                    <img className={styles.loadingSpinner} src={spinner} alt="loadingLogo"/> :
                    euroCryptoState.error ?
                    <p>somthing went wrong</p> :
                    euroCryptoState.cryptos.map (
                    crypto => <EuroCrypto   key={crypto.id} 
                                            cryptoData={crypto}
                                />  
                    )
                }
            </div>
        </div>
    );
};

export default CryptoMarket;