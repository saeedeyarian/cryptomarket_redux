import axios from "axios";
const fetchEuroCryptoRequest = () => {
    return {
        type: "FETCH_EURO_CRYPTO_REQUEST"
    }
}

const fetchEuroCryptoSuccess = crypto => {
    return {
        type: "FETCH_EURO_CRYPTO_SUCCESS",
        payload:crypto
    }
}

const fetchEuroCryptoFailur = error => {
    return {
        type: "FETCH_EURO_CRYPTO_FAILUR",
        payload:error
    }
}


const fetchEuroCrypto = () => {
    return (dispatch) => {
        dispatch (fetchEuroCryptoRequest());

        axios.get("https://api.coingecko.com/api/v3/coins/markets?vs_currency=eur&order=market_cap_desc&per_page=250&page=1&sparkline=false&price_change_percentage=24h%2C30d%2C1y%2C90d")
            .then (response => {
                const crypto = response.data
                dispatch (fetchEuroCryptoSuccess(crypto))
            })
            .catch( error => {
                const errorMsg = error.message
                dispatch(fetchEuroCryptoFailur(errorMsg))
            })
    }
}

export default fetchEuroCrypto;