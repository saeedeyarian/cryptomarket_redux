const initialState = {
    loading: false,
    cryptos : [],
    error: ""
}
const euroCryptoReducer = (state = initialState, action) => {
    switch (action.type) {
        case "FETCH_EURO_CRYPTO_REQUEST" :
            return {
                loading: true
            }
        case "FETCH_EURO_CRYPTO_SUCCESS" :
            return {
                loading: false,
                cryptos : action.payload
            }
        case "FETCH_EURO_CRYPTO_FAILUR" :
            return {
                loading: false,
                cryptos: [],
                error: action.error
            }
        default : return state
    }
}

export default euroCryptoReducer;