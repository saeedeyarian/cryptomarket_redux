import axios from "axios";
const fetchDollarCryptoRequest = () => {
    return {
        type: "FETCH_DOLLER_CRYPTO_REQUEST"
    }
}

const fetchDollarCryptoSuccess = crypto => {
    return {
        type: "FETCH_DOLLER_CRYPTO_SUCCESS",
        payload:crypto
    }
}

const fetchDollarCryptoFailur = error => {
    return {
        type: "FETCH_DOLLER_CRYPTO_FAILUR",
        payload:error
    }
}


const fetchDollarCrypto = () => {
    return (dispatch) => {
        dispatch (fetchDollarCryptoRequest());

        axios.get("https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=250&page=1&sparkline=false")
            .then (response => {
                const crypto = response.data
                dispatch (fetchDollarCryptoSuccess(crypto))
            })
            .catch( error => {
                const errorMsg = error.message
                dispatch(fetchDollarCryptoFailur(errorMsg))
            })
    }
}

export default fetchDollarCrypto;