import { combineReducers } from "redux";

import dollarCryptoReducer from  './dollar/dollarCryptoReducer';
import euroCryptoReducer from "./euro/euroCryptoReducer";

const rootReducer = combineReducers ({
    dollarCryptoState: dollarCryptoReducer ,
    euroCryptoState: euroCryptoReducer
})

export default rootReducer;