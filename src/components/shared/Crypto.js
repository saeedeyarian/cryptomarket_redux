import React from 'react';
import { Link } from 'react-router-dom';

//css
import styles from '../scss/Crypto.module.css';


const crypto = ({cryptoData}) => {


    return (
        <Link to={`/${cryptoData.id}`} style={{textDecoration:"none"}}>
            <div className={styles.container}>
                <div className={styles.nameImageContainer}>
                    <span>{cryptoData.market_cap_rank}</span>
                    <img src={cryptoData.image} alt="cryptoLogo"/>
                    <span className={styles.cryptoName}>{cryptoData.name}</span>
                </div>
                <p className={styles.symbol}>{cryptoData.symbol}</p>
                <p className={styles.price}>${cryptoData.current_price.toFixed(2)}</p>
                <p className={cryptoData.price_change_percentage_24h > 0 ? styles.priceChange : styles.redPriceChange}>{cryptoData.price_change_percentage_24h.toFixed(2)}</p>
                <p className={styles.marketCap}>${cryptoData.market_cap}</p>
            </div>
        </Link>
    );
};

export default crypto;