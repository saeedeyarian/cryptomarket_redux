import React,{ useEffect } from 'react';

//redux
import { useSelector,useDispatch } from "react-redux"

//img
import spinner from "../assets/Spinner.svg";

//components
import fetchDollarCrypto from './redux/dollar/dollarCryptoAction';
import Crypto from './shared/Crypto';

//css
import styles from "./scss/CryptoMarket.module.css";

const CryptoMarket = () => {

    const dispatch = useDispatch();
    const dollarCryptoState = useSelector(state => state.dollarCryptoState);

    useEffect( () => {
        if (!dollarCryptoState.cryptos.length ) {
            dispatch(fetchDollarCrypto())
        }
    },[])

    return (
        <div className={styles.container}>
            <div className={styles.title}>
                <p style={{width:"25%"}}>name</p>
                <p>symbol</p>
                <p>current price</p>
                <p>price change</p>
                <p>market cap</p>
            </div>
            <div classsName={styles.cryptoContainer}>
                {dollarCryptoState.loading ?
                    <img className={styles.loadingSpinner} src={spinner} alt="loadingLogo"/> :
                    dollarCryptoState.error ?
                    <p>somthing went wrong</p> :
                    dollarCryptoState.cryptos.map (
                    crypto => <Crypto key={crypto.id} 
                                        cryptoData={crypto}
                                />  
                    )
                }
            </div>
        </div>
    );
};

export default CryptoMarket;